# C Programming: A View from Space

![](/assets/space.jpg)

## Objectives

* Get exposure to a very high level view of C Programming

## The Code and Concepts

* Students are not expected to learn any of the concepts we are about to cover
* The entire purpose of this introduction is to simply expose students to C Programming... letting them come up with their own conclusions and questions
* We are going to walk through a few already created C programs, lightly talking about each as we go
* As we are walking through the code, the instructor may stop to quickly talk about a topic or something related to C programming

### 

* Labs in order:
  * hello_world.c