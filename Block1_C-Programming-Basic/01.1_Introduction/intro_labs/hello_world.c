/**********************************************************
 * ********************************************************
 * ************** Intro 1 Hello World *********************
 * ********************************************************
 * ********************************************************
 */

// Header files
#include <stdio.h>



// Program entry point
int main()
{
    // prints "Hello World" with a newline
    printf("Hello World\n");

    // Ends the program with 
    return 0;
}