# Block 1: C Programming Basics

## Sub-Blocks:

1. Introduction
2. Variables
3. Arrays and Strings
4. Input/Output Part 1
5. Operators and Expressions
6. Bitwise Operations
7. Control Flow
8. Functions
9. Error Handling, Refactoring and TDD

## Objectives:

* Introduction:
    * Have a basic understanding of programming
    * Have a basic understanding of C programming features
    * Understand how and be able to setup a C programming enviroment
    * Be able to compile your C code
* Variables:
    * Start implementing coding style
    * Understand and implement correct variable names
    * Understand and implement the different data types
    * Understand, at a basic level, sizes
    * Understand and be able to declare/initialize variables
    * Have a basic understanding of what keywords are
    * Understand and be able to convert types
* Arrays and Strings:
    * Understand and implement arrays
    * Understand and implement strings
    * Understand and implement modifying arrays/strings
* I/O Part 1:
    * Understand and implement C streams
    * Understand and implement functions
    * Understand the differences and how to implement different I/O elements, some examples:
        * getchar, putchar
        * getc, putc
        * scanf, fscanf
        * printf
        * etc
* Operations and Expressions:
    * Implement arthmetic operations
    * Implement relational operations
    * Implement logical operations
    * Implement assignment operators
    * Understand the difference between assignment and relational operators
    * Understand and implement precedence
* Bitwise Operations:
    * Understand the uses and how to implement bitwise operations
    * Understand the different numbering systems in C
    * Understand the uses and how to implement different bitwise operators
* Control Flow:
    * Understand the differences between statements and blocks and how to implement them. 
    * Understand when and how to implement conditional statements
        * if
        * else
        * else if
        * switch
    * Understand the uses of loops, when to use a specific one and how to implement them.
        * while
        * for
        * do while
    * Understand the use and how to implement break and continue
    * Understand the concepts behind nested control flow
* Functions:
    * Understand the purpose, use and benefits to functions.
    * Be able to implement functions.
    * Understand and be able to implement scope
    * Understand and implement storage class specifiers
    * Understand the use of header filels
    * Start implementing header files in larger C labs and projects
    * Understand how recursion works and be able to implement basic recursion. 
* Error Handling, Refactoring and TDD
    * Understand what it means to handle errors and be able to implement methods to do so
    * Understand how Test Driven Development works and begin implementation of it
    * Be able to identify "ugly" or invalid code and be able to refactor said code. 